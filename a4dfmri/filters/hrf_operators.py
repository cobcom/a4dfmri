import numpy as np

# Definition of constants from Khalidov et al. 2011
# Activelets: Wavelets for sparse representation of hemodynamic responses

epsilon = 0.54
tau_s = 1.54
tau_f = 2.46
tau0 = 0.98
alpha = 0.33
e0 = 0.34
v0 = 1
k1 = 7 * e0
k2 = 2
k3 = 2 * e0 - 0.2
c = (1 + ((1 - e0) * np.log(1 - e0)) / e0) / tau0

mult_coeff = 1  # multiplicative coefficient

S = v0 * epsilon / tau0 * (
            -(k1 + k2) * c * tau0 - k3 + k2)  # scaling factor has to be applied to the signal and not to the roots
gamma1 = ((k1 + k2) * ((1 - alpha) / (alpha * tau0) - c / alpha) - (k3 - k2) / tau0) / \
         (- (k1 + k2) * c * tau0 - k3 + k2)

alpha1 = mult_coeff / tau0
alpha2 = mult_coeff / (alpha * tau0)
alpha3 = mult_coeff / (2 * tau_s) * (1 + 1j * np.sqrt(4 * (tau_s ** 2) / tau_f - 1))
alpha4 = mult_coeff / (2 * tau_s) * (1 - 1j * np.sqrt(4 * (tau_s ** 2) / tau_f - 1))


def hrf(u):
    """Inputs:
    u = input signal (piece-wise constant)

    Outputs:
    This function, appplied to a Dirac impulse gives as output the BOLD response
    (HRF) following the papaer of Khalidov et al., 2011.
    """

    x1, x2, x3, x4 = 0, 0, 0, 0
    bold_linear_list = []

    for i in range(0, len(u)):
        delta_x1 = epsilon * u[i] - x1 / tau_s + x2 / tau_f
        delta_x2 = - x1
        delta_x3 = (x2 - (x3 / alpha)) / tau0
        delta_x4 = c * x2 - ((1 - alpha) / (alpha * tau0)) * x3 - x4 / tau0

        x1 = x1 + delta_x1 * mult_coeff
        x2 = x2 + delta_x2 * mult_coeff
        x3 = x3 + delta_x3 * mult_coeff
        x4 = x4 + delta_x4 * mult_coeff

        bold_linear = v0 * ((k1 + k2) * x4 + (k3 - k2) * x3)
        bold_linear_list.append(bold_linear)
    return bold_linear_list


def h_operator(x):
    """Implementation of H operator as in VDV paper
    x = input signal
    gamma1 = zero of the transfer function L
    alpha1-4 = poles of the transfer function H
    S = scaling factor"""
    y = np.zeros_like(x, dtype=np.complex)

    for n in range(1, len(x)):
        y[n] = x[n] - np.exp(-gamma1) * x[n - 1]

    for n in range(1, len(x)):
        y[n] = y[n] + np.exp(-alpha1) * y[n - 1]

    for n in range(1, len(x)):
        y[n] = y[n] + np.exp(-alpha2) * y[n - 1]

    for n in range(1, len(x)):
        y[n] = y[n] + np.exp(-alpha3) * y[n - 1]

    for n in range(1, len(x)):
        y[n] = y[n] + np.exp(-alpha4) * y[n - 1]

    y = np.real(y * S)
    return y


def hrf_operator_TR_adjusted(x, TR):
    """Implementation of H operator as in VDV paper
    x = input signal
    gamma1 = zero of the transfer function L
    alpha1-4 = poles of the transfer function H
    S = scaling factor"""
    y = np.zeros_like(x, dtype=np.complex)

    for n in range(1, len(x)):
        y[n] = x[n] - np.exp(-gamma1 * TR) * x[n - 1]

    for n in range(1, len(x)):
        y[n] = y[n] + np.exp(-alpha1 * TR) * y[n - 1]

    for n in range(1, len(x)):
        y[n] = y[n] + np.exp(-alpha2 * TR) * y[n - 1]

    for n in range(1, len(x)):
        y[n] = y[n] + np.exp(-alpha3 * TR) * y[n - 1]

    for n in range(1, len(x)):
        y[n] = y[n] + np.exp(-alpha4 * TR) * y[n - 1]

    y = np.real(y * S)
    return y
